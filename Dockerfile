FROM python:buster
WORKDIR /tmp
RUN apt update && apt install -y curl git jq bash build-essential openssl && \
    pip install awscli boto3 pelican && \
    if [ $(lscpu --json | jq '.lscpu[] | select(.field=="Architecture:") .data' -r) = "x86_64" ]; then echo 'Running x86_64 installers' && \
    curl -L -o hugo_0.82.1_Linux-64bit.tar.gz https://github.com/gohugoio/hugo/releases/download/v0.82.1/hugo_0.82.1_Linux-64bit.tar.gz && \
    tar xvf hugo_0.82.1_Linux-64bit.tar.gz hugo && chmod +x hugo && mv hugo /usr/bin/hugo && rm hugo_0.82.1_Linux-64bit.tar.gz && \
    curl -L -o /tmp/aws-sam-cli-linux-x86_64.zip  https://github.com/aws/aws-sam-cli/releases/latest/download/aws-sam-cli-linux-x86_64.zip && \
    unzip -q /tmp/aws-sam-cli-linux-x86_64.zip -d /tmp/sam-installation && \
    /tmp/sam-installation/install && rm /tmp/aws-sam-cli-linux-x86_64.zip ; fi
CMD ["/bin/bash"]

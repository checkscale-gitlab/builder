A batteries included and still small build, deploy and general purpose pipeline container.

## What's included

- alpine linux
- bash
- curl
- python 3.(latest stable)
- pip
- awscli
- boto3
- git
- jq
- pelican
- hugo

## On which platforms

- linux/amd64
- linux/arm64
- linux/arm/v7
- linux/arm/v6

## Where to get the images

- `docker.io/aapjeisbaas/builder`
- `aapjeisbaas/builder`

[Dockerhub](https://hub.docker.com/r/aapjeisbaas/builder))

## Versions

Nope, not for now at least.

## Updates

Weekly builds and security scans, at this moment there are no known vulnerabilities.

## My software is not in here

Make a fork add the software and build it yourself or open a merge request.

![Google Analytics](https://www.google-analytics.com/collect?v=1&tid=UA-48206675-1&cid=555&aip=1&t=event&ec=repo&ea=view&dp=gitlab%2Fbuilder%2FREADME.md&dt=builder)
